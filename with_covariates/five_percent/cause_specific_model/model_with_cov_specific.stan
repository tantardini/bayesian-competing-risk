/*
# =============================================================== #
        "Permanence times of bachelor Polimi students"
                Course on Bayesian Statistics
                    Politecnico di Milano
                        A.Y. 2017-2018
 
      Copyright (C) 2018 Monica Giordano & Mattia Tantardini
# =============================================================== #

This program is free software: you can redistribute it and/or 
modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program. If not, see: 
<http://www.gnu.org/licenses/>.

# -------------------------------------------------------------- #

Authors:  Monica Giordano & Mattia Tantardini
Date:     February, 2018

# =============================================================== #
# =============================================================== #
*/

/*
We provide the formulation of the model assuming that abandons and 
graduations are two independent world. We assume all the 
parameters exchangeable only within the two groups. Since also the 
overall likelihood factorizes into two different likelihood, one 
for each group, we can write a single model and then run it in 
parallel: one model giving in input data for the abandon 
cause-specific group, and one model giving in input data for the  
graduation cause-specific group.
*/

data{
    // Number of all students
    int<lower = 0> n;

    // Number of intervals in the support of the baseline hazard 
    int<lower = 0> K_model;

    /* Vector with the extremes of the intervals of the support 
    of the baseline hazard */
    vector<lower = 0>[K_model+1] a_model;

    /* Matrix of the indicators for the intervals that contain 
    failure times */
    int<lower = 0, upper = 1> N[n,K_model];

    // Matrix for the cumulative baseline hazard
    matrix<lower = 0>[n,K_model] H;

    // Number of Academic Years
    int<lower = 0> q;
    
    /* Vector of the covariates related to acedemic year of 
    enrolment */
    int<lower = 0, upper = q> AY[n];

    // Number of Courses of Study
    int<lower = 0> p;
    
    // Vector of the covariates related to course of study
    int<lower = 0, upper = p> CoS[n];
    
    // Parameter for the prior distributions of the gammas.
    real<lower = 0> w;
    
    /* Correction parameter for the exponential distribution 
    for lambda_star. Since it assigns a lot of mass to values 
    near to zero, we shift all the values by this positive 
    constant. Values of lambda_star too close to zero may induce 
    computational problems*/
    real<lower = 0> lambda_star_correction;
    
    /* Correction parameter for the zeros in the Theta matrices. 
    This is needed, otherwise zeros in Theta matrices can produce 
    derivatives (of the model) with infinite value, so that stan 
    cannot compute anything. It actually does not even manage to 
    evaluate the first iteration to initalize the model. Indeed, 
    Theta is the parameter of our Poisson likelihood, and the 
    parameter of a Poisson cannot be 0. We will add this parameter 
    to each entry of the matrix in order not to have any 0 in 
    input as parameter for the Poisson density.*/
    real<lower = 0> Theta_correction;
    
}

transformed data{
    // We compute the lengths of the intervals 
    vector<lower = 0>[K_model] lengths;

    lengths = a_model[2:K_model+1] - a_model[1:K_model];
}

parameters{
    // Vector of the levels of the baseline hazard
    vector<lower = 0>[K_model] gamma;

    /* Parameter for the priors on the gammas. It is the parameter 
    of the exponential family we want to center our baseline 
    hazard to.*/
    real<lower = 0> lambda_star;
    
    /* Parameters for the random effect on the Academic Year. We 
    have a vector for the abandon and a vector for the graduation 
    and each vector contains q parameters, each one keeping track 
    of the effect of the single Academic Year. */
    vector[q] alfa;

    // Hyperparameter for the mean of alfa_A and alfa_G.
    real alfa_bar;

    /* Paramters for the random effect on the Course of Study. We 
    have a vector for the abandon and a vector for the graduation 
    and each vector contains p parameters, each one keeping track 
    of the effect of the single Course of Study. */
    vector[p] psi;

    // Hyperparameter for the mean of alfa_A and alfa_G.
    real psi_bar;
}

transformed parameters{
    /* Shifting of lambda_star to the rigth, to prevent numerical 
    problems */
    /* Ok, quindi questa in realt?? per ora ?? di controllo, non 
    mi serve in realt??*/
    real<lower = lambda_star_correction> lambda_star_shifted;
    lambda_star_shifted = lambda_star + lambda_star_correction;
    
}

model{
    /* Local variables to represent the parameters of the Poisson 
    densities for N_A and N_G. */
    matrix[n,K_model] Theta;

    // Local variable to vectorize the priors for alfa and psi
    vector[q] ones_q;
    vector[p] ones_p;
    
    /* Local variable to store the term exp(x_i * beta_j) in the 
    matrices Theta_j. They have length n, as the total number of 
    individuals.*/
    vector[n] xbeta;

    // Initialization of the two vectors of ones
    for(i in 1:q) {ones_q[i] = 1;}
    for(i in 1:p) {ones_p[i] = 1;}
    
    // Computation of the term due to the covariates
    for(i in 1:n){
        xbeta[i] = exp(alfa[AY[i]] + psi[CoS[i]]);
    }
    
    // Computation of matrices Theta_j.
    /* Each column of Theta_j is always multiplied element-wise 
     by the term xbeta_j. */
    for(h in 1:K_model){
        Theta[:,h] = xbeta .* (H[:,h] * gamma[h]) + 
                     Theta_correction;
    }


    // Priors
    
    // Prior for lambda_star
    lambda_star ~ exponential(0.2);
    /* Shifting of lambda_star to right by lambda_star_correction. 
    This means that we have to increment the total log likelihood 
    by lambda_star * lambda_star_correction. This additional 
    line is the same as if we consider in the likelihood 
    lambda_star_shifted instead of lambda_star. */
    target += lambda_star * lambda_star_correction;
    
    // Prior for gamma: we forget about the last interval
    gamma ~ gamma(lambda_star_shifted * w * lengths, w * lengths);
    
    // Priors for the Academic Year parameters
    alfa_bar ~ normal(0, 10000);
    alfa ~ normal(alfa_bar * ones_q, 10000);

    // Priors for the Course of Study parameters
    psi_bar ~ normal(0, 10000);
    psi ~ normal(psi_bar * ones_p, 10000);

    
    // Likelihood
    
    /* Anche qui per il sample dovrebbe essere molto piu' veloce 
    non fare tutti i loop ma usare una forma vettorizzata */
    for(h in 1:K_model){
        N[:,h] ~ poisson(Theta[:,h]);
    }
    
}

generated quantities{}
