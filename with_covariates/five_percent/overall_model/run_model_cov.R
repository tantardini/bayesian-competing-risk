# =============================================================== #
#       "Permanence times of bachelor Polimi students"
#               Course on Bayesian Statistics
#                   Politecnico di Milano
#                       A.Y. 2017-2018
#
#     Copyright (C) 2018 Monica Giordano & Mattia Tantardini
# =============================================================== #
#
# This program is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see:
# <http://www.gnu.org/licenses/>.
#
# -------------------------------------------------------------- #    
#
# Authors:  Monica Giordano & Mattia Tantardini
# Date:     February, 2018
#
# =============================================================== #
# =============================================================== #


# Get the working directory. The dataset to be load must be in it.
working_dir = getwd()
source(paste(working_dir,"prepare_data_cov_5%.R", sep = "/"))

###############################################################
# Running Stan model

library(rstan)

# Options to run chains in parallel
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

### Input data
# 
# We will not pass to stan the complete matrices H_A, H_G, N_A, 
# N_G. We remove the first and the last column of these matrices.
# 1) The last column is useless, since the a_A and a_G are built 
# in such a way that in the last interval [a_A[K_A], +inf] and 
# [a_G[K_G], +inf] there are no observations. This means that we 
# cannot compute any likelihood for the data related to these 
# intervals, and so the estimate of the posteriog for gamma_A[K_A]
# and gamma_G[K_G] will be exactly the prior given in input (i.e. 
# that we set at a suitable value).
# 2) The first column is also useless for the same reason, i.e. 
# the intervals a_A and a_G are built in such a way that no 
# observation falls in the first interval. But intervals a_A and
# a_G are built using the times related to student who did not 
# have period of suspensions. Thus, it may happen that some 
# student enrolled to university has a period of 
# suspension, and then he abandoned university immediately after 
# that. That is, we may have some few observations in the first 
# interval for the H_A and N_A matrices. In this case, since the 
# number of such observations are very few, we decide not to 
# consider these data.

# Controls to properly exclude the first and the last column of 
# data matrices:
length(which(N_A[,1] == 1))
length(which(N_A[,K_A] == 1))
length(which(N_G[,1] == 1))
length(which(N_G[,K_G] == 1))

# Note for the choice of the value for the parameter w: this 
# parameter influences the a priori variance of the gamma_A and 
# gamma_G. Setting w=1 means not to consider any tuning parameter, 
# and so the a priori variance is related only to the length of 
# the support interval. Choosing w>1 means further decreasing the 
# a priori variance, while choosing w<1 means further increasing 
# the a priori variance, thus letting the value of the 
# corresponding gamma_A[h] or gamma_G[h] to assume values in a 
# larger range. 
# We choose w = 0.01.

# Now the input data
input_data = list(n = n_A+n_G+n_C,
                  K_G_model = K_G-2,
                  K_A_model = K_A-2,
                  a_G_model = a_G[2:K_G],
                  a_A_model = a_A[2:K_A],
                  N_G = N_G[,2:(K_G-1)],
                  N_A = N_A[,2:(K_A-1)],
                  H_G = H_G[,2:(K_G-1)],
                  H_A = H_A[,2:(K_A-1)],
                  q = q,
                  AY = AY,
                  p = p,
                  CoS = CoS,
                  w = 0.01,
                  lambda_star_correction = 0.01,
                  Theta_correction = 0.01)

# Some useful variables to do diagnostic and inference
n_actual_iter = 5000
n_param = K_A-2 + K_G-2 + 1 + 1 + 2*q + 2*p + 2 + 2

# Translation to C++
step1 = stanc(file = 'model_with_cov.stan', 
              model_name = 'GT_with_covariates')

# Compiling C++ code
step2 = stan_model(stanc_ret = step1)

# Sampling
tempo_inizio = proc.time()
step3 = sampling(step2,
                 data = input_data, 
                 chains = 4,
                 iter = 22500,
                 warmup = 10000,
                 thin = 10,
                 init = 'random',
                 algorithm = 'NUTS',
                 #diagnostic_file = 'diagnostic.txt',
                 seed = 2017)
tempo_fine = proc.time()
tempo = tempo_fine - tempo_inizio
tempo[3]/60/60

# Save the output of the stan fit. The name of the saved file is 
# encoded in the following way: "chain_" followed by:
# - number of chains run in parallel
# - number of total iteration per chain
# - number of burn-in iterations
# - thinning
# Each number is separed by an underscore and the thousands are 
# abbreviated with "k"
save(step3, n_actual_iter, n_param, 
     file = 'chain_4_22k_10k_10_cov.Rdata')


