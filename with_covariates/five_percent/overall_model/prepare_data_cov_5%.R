# =============================================================== #
#       "Permanence times of bachelor Polimi students"
#               Course on Bayesian Statistics
#                   Politecnico di Milano
#                       A.Y. 2017-2018
#
#     Copyright (C) 2018 Monica Giordano & Mattia Tantardini
# =============================================================== #
#
# This program is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see:
# <http://www.gnu.org/licenses/>.
#
# -------------------------------------------------------------- #    
#
# Authors:  Monica Giordano & Mattia Tantardini
# Date:     February, 2018
#
# =============================================================== #
# =============================================================== #


# Get the working directory. The dataset to be load must be in it.
working_dir = getwd()

# Loading the dataset containing 5% of the whole data. A variable 
# named "data_11" will be created.
load(paste(working_dir,"campione_proporzionale.Rdata", sep = "/"))
Data = data_11
rm(data_11)

# Some controls on the dataset:
dim(Data)
names(Data)

# We have to preprocess this data to select only some variables.
# In particular, we need:
# - The time of permanence in months at Politecnico (named Time)
# - The state of the student (named Student_State):
#   * Student_State = 0 if the student is still active;
#   * Student_State = 1 if the student graduated (G);
#   * Student_State = 2 if the student abandoned the studies (A);
# - The censoring index (named Delta): =0 if the student is 
#   censred, i.e. still active, =1 if the student dropped out, 
#   for graduation or for abandon of studies
# - The vector of the covariates. In our case we select only two 
#   covariates: the Academic Year of Enrolment (named 
#   Academic_Year), and the Course of Study (named Course_of_Study)

# This is the new dataframe we will use in our project:
GTData = data.frame(
    Time = Data$y,
    Student_State = Data$SITUAZIONE_STUDENTE,
    Delta = Data$DATO_ESATTO,
    Academic_Year = Data$AA_IMMAT,
    Course_of_Study = Data$NOME_CDS,
    Month_Suspension = Data$MESI_SOSPENSIONE
)
rm(Data)

print("Dataset dimensions:")
print(dim(GTData))
print("Dataset colnames:")
print(colnames(GTData))

# Checking for non-positive times:
print("Checking: are there non-positive times among graduated?")
print(length(which(GTData$Time < 0)) != 0)

# We extract indices for the situation of the student
index_active = which(GTData$Student_State == 0) # == index_censored
index_graduated = which(GTData$Student_State == 1)
index_abandon = which(GTData$Student_State == 2)

# Check for consistency:
length(index_abandon) + length(index_active) + 
    length(index_graduated) == dim(GTData)[1]

##################################################################
# Preparing data to be passed to stan

# Note: we will use the following suffices for most of our variables:
# - _A: denotes variables related to students who abandoned studies.
#       It corresponds to Student_State = 2 and Delta = 1;
# - _G: denotes variables related to students who graduated. It 
#       corresponds to Student_State = 1 and Delta = 1;
# - _C: denotes variables related to censored students, i.e. 
#       students who are still active. It corresponds to 
#       Student_State = 0 and Delta = 0.

Time_A = GTData$Time[index_abandon]
Time_G = GTData$Time[index_graduated]
Time_C = GTData$Time[index_active]

n = dim(GTData)[1]
n_A = length(Time_A)
n_G = length(Time_G)
n_C = length(Time_C)

###################################################################
## Definition of a_A, a_G, vectors of the extremes of the intervals 
## for the support of the baseline hazard

# We have to consider only unique times and to throw away the times 
# related to students who had a period of suspension.

# Identifying students who had a suspensions
sosp_A = GTData$Month_Suspension[index_abandon]
sosp_G = GTData$Month_Suspension[index_graduated]
sosp_A[which(is.na(sosp_A))] = 0
sosp_G[which(is.na(sosp_G))] = 0

# Extraction of unique times of students without suspensions, and 
# ordering them in ascending order
unique_Time_A = sort(unique(Time_A[which(sosp_A == 0)]), decreasing = F)
unique_Time_G = sort(unique(Time_G[which(sosp_G == 0)]), decreasing = F)

# Creation of the vector of all unique death" times, augmented by 
# zero and the largest censoring time:
d_A = c(0, unique_Time_A, max(Time_C))
d_G = c(0, unique_Time_G, max(Time_C))

# Creation of the vector with the extremes of the intervals
K_A = length(d_A) + 1
a_A = numeric(K_A)
a_A[1] = 0
a_A[2:(K_A-1)] = .5 * (d_A[1:(K_A-2)] + d_A[2:(K_A-1)])
a_A[K_A] = d_A[K_A-1] + .5 * (d_A[K_A-1] - d_A[K_A-2])
a_A

K_G = length(d_G) + 1
a_G = numeric(K_G)
a_G[1] = 0
a_G[2:(K_G-1)] = .5 * (d_G[1:(K_G-2)] + d_G[2:(K_G-1)])
a_G[K_G] = d_G[K_G-1] + .5 * (d_G[K_G-1] - d_G[K_G-2])
a_G

rm(d_A, d_G, unique_Time_A, unique_Time_G, sosp_A, sosp_G)

################################################################
### Construction of the matrices H_A, H_G, and N_A, N_G.
### H is the matrix that helps to compute the cumulative baseline 
### hazard, while N is the matrix that keeps track of the interval 
### of the baseline hazard where each death time falls.

# We build submatrices for the censored data, to be appended to 
# the two matrices of the exact data.
# 
# Note that, from the competing risks model, if we are analysing 
# the abandon times, we have to consider as censored times both the 
# real censored times and all the graduation times; while, if we are 
# analysing the graduation times, we consider as censored both the 
# real censored times and all the abandon times. This is beacuse, 
# if for instance we are interested in the abandon times, if a 
# student drops out because of graduation, he has not dropped out 
# because of abandon, and so it is the same as an active student: 
# for the abandon times, the censoring index is 1 if he dropped 
# out beacuse of abandon and 0 if not; then, if a student graduates, 
# is censoring index (for the abandon times) is 0 (as a "true" 
# censored student), because he did not drop out because of abandon.
# The same reasoning for graduation.
# 
# Then, we call H_C_forA and N_C_forA the matrices that consider 
# graduation times and true censored times as censored time (to be 
# appended to H_A and N_A), and H_C_forG and N_C_forG the matrices 
# that consider abandon times and true censored times as censored 
# times (to be appended to H_G and N_G).
# 
# Note also that we don't need to build the two matrices N for the 
# same reason: N_C_forA contains all the true censored and the 
# graduation times, that are censored from the point of view of 
# abandon times. An analogous reasoning for N_C_forG

H_C_forA = matrix(data = NA, nrow = n_C+n_G, ncol = K_A)
N_C_forA = matrix(data = 0, nrow = n_C+n_G, ncol = K_A)
# We define the augmented vectors of censored times:
Time_CG = c(Time_C, Time_G)
for(i in 1:length(Time_CG)){
    # h_C_forA = index for the interval in which the failure 
    # time occurs. Maybe a censored data can occur exactly at one 
    # extreme of some interval, but H and N are defined in such a 
    # way that this is not a problem.
    h_C_forA = findInterval(Time_CG[i],a_A)
    if(h_C_forA == 1){
        H_C_forA[i,] = c(Time_CG[i]-a_A[h_C_forA], rep(0,K_A-1))
    } else if (h_C_forA == K_A){
        h_inf = 1:(K_A-1)
        H_C_forA[i,] = c(a_A[h_inf+1]-a_A[h_inf], 
                        Time_CG[i]-a_A[h_C_forA])
    } else {
        h_inf = 1:(h_C_forA-1)
        H_C_forA[i,] = c(a_A[h_inf+1]-a_A[h_inf], 
                        Time_CG[i]-a_A[h_C_forA], 
                        rep(0, K_A-h_C_forA))
    }
}

H_C_forG = matrix(data = NA, nrow = n_C+n_A, ncol = K_G)
N_C_forG = matrix(data = 0, nrow = n_C+n_A, ncol = K_G)
# We define the augmented vectors of censored times:
Time_CA = c(Time_C, Time_A)
for(i in 1:length(Time_CA)){  
    # h_C_forG = index for the interval in which the failure 
    # time occurs. Maybe a censored data can occur exactly at one 
    # extreme of some interval, but H and N are defined in such a 
    # way that this is not a problem.
    h_C_forG = findInterval(Time_CA[i],a_G)
    if(h_C_forG == 1){
        H_C_forG[i,] = c(Time_CA[i]-a_G[h_C_forG], rep(0,K_G-1))
    } else if (h_C_forG == K_G){
        h_inf = 1:(K_G-1)
        H_C_forG[i,] = c(a_G[h_inf+1]-a_G[h_inf], 
                      Time_CA[i]-a_G[h_C_forG])
    } else {
        h_inf = 1:(h_C_forG-1)
        H_C_forG[i,] = c(a_G[h_inf+1]-a_G[h_inf], 
                         Time_CA[i]-a_G[h_C_forG], 
                         rep(0, K_G-h_C_forG))
    }
}

H_A = matrix(data = NA, nrow = n_A, ncol = K_A)
N_A = matrix(data = NA, nrow = n_A, ncol = K_A)
for (i in 1:n_A){
    # h = index for the interval in which the failure time occurs.
    # It cannot happen that a "failure" time occurs at an extreme 
    # of some interval because we defined the extreme to make this 
    # not happen.
    h = findInterval(Time_A[i],a_A) #max(which(a_A < Time_A[i]))
    if(h == 1){
        H_A[i,] = c(Time_A[i]-a_A[h], rep(0,K_A-1))
        N_A[i,] = c(1, rep(0,K_A-1))
    } else if (h == K_A){
        h_inf = 1:(K_A-1)
        H_A[i,] = c(a_A[h_inf+1]-a_A[h_inf], Time_A[i]-a_A[h])
        N_A[i,] = c(rep(0,length(h_inf)), 1)
    } else {
        h_inf = 1:(h-1)
        h_sup = (h+1):K_A
        H_A[i,] = c(a_A[h_inf+1]-a_A[h_inf], Time_A[i]-a_A[h], 
                    rep(0,length(h_sup)))
        N_A[i,] = c(rep(0,length(h_inf)), 1, rep(0,length(h_sup)))
    }
}
# Binding data for students who abandoned with censored data
H_A = rbind(H_A,H_C_forA)
N_A = rbind(N_A,N_C_forA)

H_G = matrix(data = NA, nrow = n_G, ncol = K_G)
N_G = matrix(data = NA, nrow = n_G, ncol = K_G)
for (i in 1:n_G){
    # h = index for the interval in which the failure time occurs.
    # It cannot happen that a "failure" time occurs at an extreme 
    # of some interval because we defined the extreme to make this 
    # not happen.
    h = findInterval(Time_G[i],a_G) #max(which(a_A < Time_A[i]))
    if(h == 1){
        H_G[i,] = c(Time_G[i]-a_G[h], rep(0,K_G-1))
        N_G[i,] = c(1, rep(0,K_G-1))
    } else if (h == K_G){
        h_inf = 1:(K_G-1)
        H_G[i,] = c(a_G[h_inf+1]-a_G[h_inf], Time_G[i]-a_G[h])
        N_G[i,] = c(rep(0,length(h_inf)), 1)
    } else {
        h_inf = 1:(h-1)
        h_sup = (h+1):K_G
        H_G[i,] = c(a_G[h_inf+1]-a_G[h_inf], Time_G[i]-a_G[h], 
                    rep(0,length(h_sup)))
        N_G[i,] = c(rep(0,length(h_inf)), 1, rep(0,length(h_sup)))
    }
}
# Binding data for students who graduated with censored data
H_G = rbind(H_G,H_C_forG)
N_G = rbind(N_G,N_C_forG)

#### Removing useless variables
rm(H_C_forA,H_C_forG,N_C_forA,N_C_forG, h_C_forA, h_C_forG, h_inf,
   h_sup, Time_CA, Time_CG)

##################################################################
# Preparing covariates to be passed to the stan model.
# 
# We need to convert each level value in "Academic_Year" and 
# "Course_of_Study" to an integer: this is because the stan model 
# can run much faster and in a smart way. This also allows to get 
# an immediate correspondence between each academic year and 
# course of study to the related coefficient in the regression 
# term.

# We prapare the vector of the covariate "Course_of_Study":
# - aerospaziale / aerospace ----------------------> 1 
# - ambiente e territorio /                     ---> 2
# - automazione / automation ----------------------> 3
# - biomedica / biomedical ------------------------> 4
# - chimica / chemical ----------------------------> 5
# - civile / civil --------------------------------> 6
# - edile /                                     ---> 7
# - elettrica / electrical ------------------------> 8
# - elettronica / electronic ----------------------> 9
# - energetica / energy ---------------------------> 10
# - fisica / physics ------------------------------> 11
# - gestionale / managment ------------------------> 12
# - industriale / industrial production -----------> 13
# - informatica /                               ---> 14
# - matematica / mathematical ---------------------> 15
# - materiali / materials and nanotecnology -------> 16
# - meccanica / mechanical ------------------------> 17
# - telecomunicazioni / telecommunications --------> 18
# - trasporti / transport -------------------------> 19
p = length(unique(GTData$Course_of_Study))
CoS = vector('integer', length = n)
CoS[which(GTData$Course_of_Study == 'aerospaziale')] = 1
CoS[which(GTData$Course_of_Study == 'ambiente e territorio')] = 2
CoS[which(GTData$Course_of_Study == 'automazione')] = 3
CoS[which(GTData$Course_of_Study == 'biomedica')] = 4
CoS[which(GTData$Course_of_Study == 'chimica')] = 5
CoS[which(GTData$Course_of_Study == 'civile')] = 6
CoS[which(GTData$Course_of_Study == 'edile')] = 7
CoS[which(GTData$Course_of_Study == 'elettrica')] = 8
CoS[which(GTData$Course_of_Study == 'elettronica')] = 9
CoS[which(GTData$Course_of_Study == 'energetica')] = 10
CoS[which(GTData$Course_of_Study == 'fisica')] = 11
CoS[which(GTData$Course_of_Study == 'gestionale')] = 12
CoS[which(GTData$Course_of_Study == 'industriale')] = 13
CoS[which(GTData$Course_of_Study == 'informatica')] = 14
CoS[which(GTData$Course_of_Study == 'matematica')] = 15
CoS[which(GTData$Course_of_Study == 'materiali')] = 16
CoS[which(GTData$Course_of_Study == 'meccanica')] = 17
CoS[which(GTData$Course_of_Study == 'telecomunicazioni')] = 18
CoS[which(GTData$Course_of_Study == 'trasporti')] = 19

# We prepare the vector of the covariate "Academic_Year":
# - 2001 ---> 1
# - 2002 ---> 2
# .......
# - 2011 ---> 11
q = length(unique(GTData$Academic_Year))
AY = vector('integer', length = n)
AY[which(GTData$Academic_Year == 2001)] = 1
AY[which(GTData$Academic_Year == 2002)] = 2
AY[which(GTData$Academic_Year == 2003)] = 3
AY[which(GTData$Academic_Year == 2004)] = 4
AY[which(GTData$Academic_Year == 2005)] = 5
AY[which(GTData$Academic_Year == 2006)] = 6
AY[which(GTData$Academic_Year == 2007)] = 7
AY[which(GTData$Academic_Year == 2008)] = 8
AY[which(GTData$Academic_Year == 2009)] = 9
AY[which(GTData$Academic_Year == 2010)] = 10
AY[which(GTData$Academic_Year == 2011)] = 11
