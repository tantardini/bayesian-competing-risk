/*
# =============================================================== #
        "Permanence times of bachelor Polimi students"
                Course on Bayesian Statistics
                    Politecnico di Milano
                        A.Y. 2017-2018
 
      Copyright (C) 2018 Monica Giordano & Mattia Tantardini
# =============================================================== #

This program is free software: you can redistribute it and/or 
modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program. If not, see: 
<http://www.gnu.org/licenses/>.

# -------------------------------------------------------------- #

Authors:  Monica Giordano & Mattia Tantardini
Date:     February, 2018

# =============================================================== #
# =============================================================== #
*/


data{
    // Number of all students
    int<lower = 0> n;

    /* Number of intervals in the support of the baseline hazard 
    used in this model. They are the number of columns of H_G, 
    N_G, H_A, N_A. */
    int<lower = 0> K_G_model;
    int<lower = 0> K_A_model;
    
    // Vectors with the extremes of the intervals
    vector<lower = 0>[K_G_model+1] a_G_model;
    vector<lower = 0>[K_A_model+1] a_A_model;
    
    /* Matrices of the indicators for the intervals that contain 
    failure times */
    int<lower = 0, upper = 1> N_G[n,K_G_model];
    int<lower = 0, upper = 1> N_A[n,K_A_model];
    
    // Matrices for the cumulative baseline hazard
    matrix<lower = 0>[n,K_G_model] H_G;
    matrix<lower = 0>[n,K_A_model] H_A;
    
    // Parameter for the prior distributions of the gammas.
    real<lower = 0> w;
    
    /* Correction parameter for the exponential distribution 
    for lambda_star. Since it assigns a lot of mass to values 
    near to zero, we shift all the values by this positive 
    constant. Values of lambda_star too close to zero may induce 
    computational problems. */
    real<lower = 0> lambda_star_correction;
    
    /* Correction parameter for the zeros in the Theta matrices. 
    This is needed, otherwise zeros in Theta matrices can produce 
    derivatives (of the model) with infinite value, so that stan 
    cannot compute anything. It actually does not even manage to 
    evaluate the first iteration to initalize the model. Indeed, 
    Theta is the parameter of our Poisson likelihood, and the 
    parameter of a Poisson cannot be 0. We will add this parameter 
    to each entry of the matrix in order not to have any 0 in 
    input as parameter for the Poisson density. */
    real<lower = 0> Theta_correction;
    
}

transformed data{
    // We compute the lengths of the intervals 
    vector<lower = 0>[K_G_model] lengths_G;
    vector<lower = 0>[K_A_model] lengths_A;
    
    lengths_G = a_G_model[2:K_G_model+1] - a_G_model[1:K_G_model];
    lengths_A = a_A_model[2:K_A_model+1] - a_A_model[1:K_A_model];
}

parameters{
    // Vectors of the levels of the baseline hazards
    vector<lower = 0>[K_G_model] gamma_G;
    vector<lower = 0>[K_A_model] gamma_A;
    
    /* Parameter for the priors on the gammas. It is the parameter 
    of the exponential family we want to center our baseline 
    hazard to.*/
    real<lower = 0> lambda_star;
}

transformed parameters{
    /* Shifting of lambda_star to the rigth, to prevent numerical 
    problems */
    real<lower = lambda_star_correction> lambda_star_shifted;
    
    lambda_star_shifted = lambda_star + lambda_star_correction;
}

model{
    /* Local variables to represent the parameters of the Poisson 
    densities for N_A and N_G. */
    matrix[n,K_G_model] Theta_G;
    matrix[n,K_A_model] Theta_A;

    for(h in 1:K_G_model){
        Theta_G[:,h] = H_G[:,h] * gamma_G[h] + Theta_correction;
    }
    for(h in 1:K_A_model){
        Theta_A[:,h] = H_A[:,h] * gamma_A[h] + Theta_correction;
    }
    
    
    // Priors
    
    // Prior for lambda_star
    lambda_star ~ exponential(0.2);
    /* Shifting of lambda_star to right by lambda_star_correction. 
    This mean we have to increment the total log likelihood by 
    lambda_star * lambda_star_correction. This additional 
    line is the same as consider in the likelihood 
    lambda_star_shifted instead of lambda_star. */
    target += lambda_star * lambda_star_correction;
    
    // Prior for gamma: we forget about the last interval
    gamma_G ~ gamma(lambda_star_shifted * w * lengths_G, 
                    w * lengths_G);
    gamma_A ~ gamma(lambda_star_shifted * w * lengths_A, 
                    w * lengths_A);
    
    
    // Likelihood
    for(h in 1:K_G_model){ N_G[:,h] ~ poisson(Theta_G[:,h]); }
    for(h in 1:K_A_model){ N_A[:,h] ~ poisson(Theta_A[:,h]); }
    
}

generated quantities{}
