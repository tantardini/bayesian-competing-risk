# Analysis of permanece times of students at Politecnico di Milano
### Course on Bayesian Statistics
#### *Politecnico di Milano* (ITALY)

**Author** : Monica Giordano & Mattia Tantardini 

**Mailto** : _mailMonica_, mtantar@hotmail.it

**Date**   : February 2018

-----------------------------------------------
## The project
-----------------------------------------------
The projects' aim is to analyse the permanece times of undergraduated students at Politecnico di Milano due to graduation or dropping out.
A competing risk model, that takes into account for the two different causes of exit from the university, is used to make inference in the framework of survival analysis, from a bayesian point of view.

-----------------------------------------------
## Documentation
-----------------------------------------------
All the documentation about the models used, some technical remarks about the code and the whole analysis of the results are contained in the `Report.pdf`.

-----------------------------------------------
## Code
-----------------------------------------------

### Folder's structure

There are two folders in the repository:
- `with covariates`: code abput models with covariates
- `without covariates`: code about models without covariates

Each one of it contains two subfolders:
- `5%`: contains code to run models and to do analysis on the 5% of the whole dataset of students
- `all`: contains code to run models and to do analysis on the whole dataset of students

In each leaf folder are contained (`_x`=`_5%` or `_x`=`_all`):
- the proper dataset
- `prepare_data_x.R`: script file that defines the data structure to be used by the model
- `diagnostic_and_analysis_x.R`: script file to do diagnostic and analysis on the obtained chains
- `model_*.stan`: Stan model
- `run_model.R`: script to run the Stan model

### Installation

We use Stan to implement our theoretical models and to sample from the posterior of the parameters we want to make inference on. To properly run the code, you can follow the instructions at <https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started>. This is specific to install and run Stan, but also guide you in the installation of all other software requirements that you need.

### Running the code

Enter the chosen subfolder. If you do not have a sampled chain yet, open `run_model.R` and run it (be sure to check all the file paths and the parameters for the sampling). Otherwise, if you already have a sampled chain, you can directly run `diagnostic_and_analysis_x.R` to see all the analysis done.

**Note**: we do our work running all the Stan models from RStudio. Another way to run the Stan model is through the terminal, and this way should be considerably faster. For further information on how to run Stan models from the terminal, see <https://github.com/stan-dev/cmdstan>.

-----------------------------------------
##  DEV ENVIRONMENT
----------------------------------------
OS         : Windows 10 Home 64-bit

Processor  : AMD A10-9600P RADEON R5, 10 COMPUTE CORES 4C+6G 2.40GHz

RAM        : 16 GB

R x64      : 3.4.2

RStudio    : 1.1.383

Rtools     : 3.4

rstan      : 2.16.2 (package of R)

